mc20_13TeV.504330.aMCPy8EG_NNPDF30NLO_A14N23LO_ttee.deriv.DAOD_PHYSLITE.e8255_s3681_r13144_p6266
mc20_13TeV.504330.aMCPy8EG_NNPDF30NLO_A14N23LO_ttee.deriv.DAOD_PHYSLITE.e8255_s3681_r13145_p6266
mc20_13TeV.504330.aMCPy8EG_NNPDF30NLO_A14N23LO_ttee.deriv.DAOD_PHYSLITE.e8255_s3681_r13167_p6266

mc20_13TeV.504334.aMCPy8EG_NNPDF30NLO_A14N23LO_ttmumu.deriv.DAOD_PHYSLITE.e8255_s3681_r13144_p6266
mc20_13TeV.504334.aMCPy8EG_NNPDF30NLO_A14N23LO_ttmumu.deriv.DAOD_PHYSLITE.e8255_s3681_r13145_p6266
mc20_13TeV.504334.aMCPy8EG_NNPDF30NLO_A14N23LO_ttmumu.deriv.DAOD_PHYSLITE.e8255_s3681_r13167_p6266

# mc20_13TeV.504342.aMCPy8EG_NNPDF30NLO_A14N23LO_tttautau.deriv.DAOD_PHYSLITE.e8255_s3681_r13144_p6266
# mc20_13TeV.504342.aMCPy8EG_NNPDF30NLO_A14N23LO_tttautau.deriv.DAOD_PHYSLITE.e8255_s3681_r13145_p6266
# mc20_13TeV.504342.aMCPy8EG_NNPDF30NLO_A14N23LO_tttautau.deriv.DAOD_PHYSLITE.e8255_s3681_r13167_p6266
mc20_13TeV.504342.aMCPy8EG_NNPDF30NLO_A14N23LO_tttautau.deriv.DAOD_PHYSLITE.e8255_s3797_r13144_p6266
mc20_13TeV.504342.aMCPy8EG_NNPDF30NLO_A14N23LO_tttautau.deriv.DAOD_PHYSLITE.e8255_s3797_r13145_p6266
mc20_13TeV.504342.aMCPy8EG_NNPDF30NLO_A14N23LO_tttautau.deriv.DAOD_PHYSLITE.e8255_s3797_r13167_p6266

mc20_13TeV.504338.aMCPy8EG_NNPDF30NLO_A14N23LO_ttZqq.deriv.DAOD_PHYSLITE.e8255_s3681_r13144_p6266
mc20_13TeV.504338.aMCPy8EG_NNPDF30NLO_A14N23LO_ttZqq.deriv.DAOD_PHYSLITE.e8255_s3681_r13145_p6266
mc20_13TeV.504338.aMCPy8EG_NNPDF30NLO_A14N23LO_ttZqq.deriv.DAOD_PHYSLITE.e8255_s3681_r13167_p6266

mc20_13TeV.504346.aMCPy8EG_NNPDF30NLO_A14N23LO_ttZnunu.deriv.DAOD_PHYSLITE.e8255_s3681_r13144_p6266
mc20_13TeV.504346.aMCPy8EG_NNPDF30NLO_A14N23LO_ttZnunu.deriv.DAOD_PHYSLITE.e8255_s3681_r13145_p6266
mc20_13TeV.504346.aMCPy8EG_NNPDF30NLO_A14N23LO_ttZnunu.deriv.DAOD_PHYSLITE.e8255_s3681_r13167_p6266

# makred as "alternative", no baseline ttW on CentralPage
mc20_13TeV.410155.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW.deriv.DAOD_PHYSLITE.e5070_s3681_r13144_p6266
mc20_13TeV.410155.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW.deriv.DAOD_PHYSLITE.e5070_s3681_r13145_p6266
mc20_13TeV.410155.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW.deriv.DAOD_PHYSLITE.e5070_s3681_r13167_p6266

# Drell-Yan below
mc20_13TeV.410276.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13144_p6266
mc20_13TeV.410276.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13145_p6266
mc20_13TeV.410276.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13167_p6266

mc20_13TeV.410277.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13144_p6266
mc20_13TeV.410277.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13145_p6266
mc20_13TeV.410277.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13167_p6266

mc20_13TeV.410278.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13144_p6266
mc20_13TeV.410278.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13145_p6266
mc20_13TeV.410278.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau_mll_1_5.deriv.DAOD_PHYSLITE.e6087_s3681_r13167_p6266

